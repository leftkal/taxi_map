package com.company;

public class Node {

    private Location loc;
    private int id;

    public Node(String[] line) {
        loc = new Location(Double.parseDouble(line[1]), Double.parseDouble(line[0]));
        id = Integer.parseInt(line[2]);
    }

    public Location getLocation() {
        return loc;
    }

    public int getId(){
        return id;
    }

}

