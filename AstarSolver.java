package com.company;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.System.exit;


public class AstarSolver {
    private Location startingPoint;
    private Location endingPoint;
    private HashMap<Location, ArrayList<Location>> myMap;
    private HashMap<Location, Double> visited = new HashMap<>();
    private Priority forehead;

    public AstarSolver(Location startingPoint, Location endingPoint, HashMap<Location, ArrayList<Location>> myMap) {
        this.startingPoint = startingPoint;
        this.endingPoint = endingPoint;
        this.myMap = myMap;
    }

    public AstarResultSet doSolve() {
        ArrayList<Location> currentNeighbours = myMap.get(startingPoint);
        forehead = new Priority(1000);

        State currentState = new State(startingPoint, 0.0, endingPoint, new State(new Location(0.0,0.0), 0.0, endingPoint));

        visited.put(new Location(currentState.getLocation()), currentState.getHeuristic());
        forehead.addArrayList(currentNeighbours, currentState, endingPoint);
        while (!(currentState.getX() == endingPoint.x) || !(currentState.getY() == (endingPoint.y))) {
            currentState = forehead.get();
            //System.out.println(forehead.mylist.size());
            //System.out.println(currentState.getLocation().x + "," + currentState.getLocation().y);
            if (!visited.containsKey(currentState.getLocation()))
                visited.put(currentState.getLocation(), currentState.getHeuristic());
            else if (visited.get(currentState.getLocation()) < currentState.getHeuristic()) {
                //System.out.println("i've been here");
                continue;
            }
            else {
                visited.put(currentState.getLocation(), currentState.getHeuristic());
            }

            currentNeighbours = new ArrayList<>(myMap.get(currentState.getLocation()));

            ArrayList<Location> neighboursImpersonator = new ArrayList<>(currentNeighbours);
            for (Location n : neighboursImpersonator) {
                if (visited.containsKey(n)) {
                    if (visited.get(n) < currentState.getDistanceToHere() + currentState.getLocation().distance(n) + n.distance(endingPoint)) {
                        currentNeighbours.remove(n);
                    }
                }
            }
            forehead.addArrayList(currentNeighbours, currentState, endingPoint);
        }
        return new AstarResultSet(currentState);
    }
}
