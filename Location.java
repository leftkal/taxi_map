package com.company;

public class Location {
    public double x;
    public double y;

    public Location(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Location(Location loc) {
        this.x = loc.x;
        this.y = loc.y;
    }

    public double distance (Location other) {
        final int EARTH_RADIUS = 6371; // Approx Earth radius in KM
        double startLat = this.x;
        double startLong = this.y;
        double endLat = other.x;
        double endLong = other.y;
        double dLat  = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));
        startLat = Math.toRadians(startLat);
        endLat   = Math.toRadians(endLat);
        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(startLat) * Math.cos(endLat) * Math.pow(Math.sin(dLong / 2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c; // <-- d
    }

    private double degreesToRadians(double degrees) {
        return degrees * Math.PI / 180;
    }
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Location))
            return false;

        Location other = (Location)o;
        if (this == other)
            return true;
        return (this.x == other.x && this.y == other.y);
    }

    @Override
    public int hashCode() {
        int result =(int)(10000000*this.x+10000000*this.y);
        return result;
    }
}
