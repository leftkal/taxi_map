package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.System.exit;

public class Main {

    public static Location findClosest(Location k, ArrayList<Location> positions) {
        double minDistance = Double.MAX_VALUE;
        Location result = new Location(0.0, 0.0);
        for(Location i: positions) {
            if(i.distance(k) < minDistance) {
                result = new Location(i);
                minDistance = i.distance(k);
            }
        }
        return result;
    }

    public static void main(String[] args) {

        ArrayList<Location> nodes = new ArrayList<>();  //an ArrayList to store all the Geo-Coordinates
        HashMap <Location,ArrayList<Location>> neighbors = new HashMap<>();  //put the neighbors in a map
        ArrayList<Location> taxis;
        Location client;
        try (BufferedReader br = new BufferedReader(new FileReader("nodes.csv"))) {
            String line;  //first line is useless
            line = br.readLine();
            line = br.readLine();  //read first node
            String[] str = line.split(","); // use comma as separator
            Node k = new Node(str);
            Location previous_loc = k.getLocation();
            nodes.add(previous_loc);
            int previous_id = k.getId();
            neighbors.put(previous_loc, new ArrayList<>());
            while ((line = br.readLine()) != null) {
                str = line.split(",");
                Node l = new Node(str);
                Location current_loc = l.getLocation();
                nodes.add(current_loc);
                Integer current_id = l.getId();
                if (previous_id==current_id) {
                    neighbors.get(previous_loc).add(current_loc);
                    if(!neighbors.containsKey(current_loc)) {
                        neighbors.put(current_loc, new ArrayList<>());
                    }
                    neighbors.get(current_loc).add(previous_loc);
                }
                else
                    neighbors.put(current_loc, new ArrayList<>());
                previous_loc = current_loc;
                previous_id = current_id;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new FileReader("client.csv"))) {
            String line;
            line = br.readLine();
            line = br.readLine();  //read first node
            String[] str = line.split(","); // use comma as separator
            client = new Location(Double.parseDouble(str[1]), Double.parseDouble(str[0]));
        }
        catch (IOException e) {
            client = new Location(0.0, 0.0);
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new FileReader("taxis.csv"))) {
            taxis = new ArrayList<>();
            String line;
            line = br.readLine();  //read first node
            String[] str; // use comma as separator
            while ((line = br.readLine()) != null) {
                str = line.split(",");
                Node l = new Node(str);
                taxis.add(l.getLocation());
            }
        }
        catch (IOException e) {
            taxis = new  ArrayList<Location>();
            e.printStackTrace();
        }

        Location endingPoint = findClosest(client, nodes);
        AstarSolver mySolver;
        ArrayList<AstarResultSet> results = new ArrayList<>();
        for (Location i: taxis) {
            Location startingPoint = findClosest(i, nodes);
            System.out.println(i.x + " " + i.y);
            //ArrayList<Location> kappa = neighbors.get(startingPoint);
            //System.out.println(kappa.size());
            mySolver = new AstarSolver(startingPoint, new Location(endingPoint), new HashMap<>(neighbors));
            results.add(mySolver.doSolve());
            break;
        }

        results.sort(new AstarResultSetComparator());
        System.out.println(results.get(0).getDistanceTravelled());
        for(Location i: results.get(0).getRoute()) {
            System.out.println(i.x + "," + i.y);
        }

    }

}
